# Makefile

build:
	docker build -t demo1 ./
	docker build -t demo2 ./

run:
	docker network create demo-net || true
	docker run -it --name demo1 --rm --detach --publish 8080:80 --network demo-net -v mes-data:/data demo1
	docker run -it --name demo2 --rm --detach --network demo-net -v mes-data:/data demo2

kill:
	docker kill demo1 || true
	docker kill demo2 || true
	docker rm demo1 || true
	docker rm demo2 || true
	docker network rm demo-net || true
